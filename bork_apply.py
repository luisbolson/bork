#!/usr/bin/env python3
import argparse
import logging
import bork
import yaml, yamlordereddictloader
import pprint
import os

if os.geteuid() != 0:
    exit("You need root privileges to run this script. Try 'sudo bork_apply'")

pp = pprint.PrettyPrinter(indent=2)
logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)s %(message)s')

def log(text='',status='ok',obj=''):
    logging.info(bork.log_global(status,'BORK',obj,text))

def validate_config(config):
    valid_config = {
        'package': ['type','state','notify'],
        'service': ['type','status'],
        'file': ['type','source','content','owner','group','mode','notify']
    }
    ret = 0
    for item_name,item in config.items():
        if 'type' not in item.keys():
            log('Entry '+item_name+': "type" attribute not present','error',item_name)
            ret += 1
        elif item['type'] not in valid_config.keys():
            log('Type "'+item['type']+'" is not a valid type','error',item_name)
            ret += 1
        else:
            for item_key in item.keys():
                if item_key not in valid_config[item['type']]:
                    log('Attribute "'+item_key+'" is not valid for type "'+item['type']+'"','error',item_name)
                    ret += 1
    return ret

def apply_config(config):
    #Each key of config is an object item
    for item_name,item in config.items():
        # Remove entry type from dict to pass the rest as parameters
        item_type = item.pop('type')
        if item_type == 'service':
            s = bork.Service(item_name,**item)
            s.apply_config()
        elif item_type == 'file':
            f = bork.File(item_name,**item)
            f.apply_config()
        elif item_type == 'package':
            f = bork.Package(item_name,**item)
            f.apply_config()

# Create a parser
parser = argparse.ArgumentParser()
# Config File is the positional argument
parser.add_argument("file", help="Configuration file to be applied in this host")

# Parse the arguments
args = parser.parse_args()

# Get config file name
config_file = args.file

# Load YAML file
config = yaml.load(open(config_file, 'r'),Loader=yamlordereddictloader.Loader)

#pp.pprint(config)
log('Validating Config File: '+config_file)
num_errors = validate_config(config)

if num_errors:
    log('Number of errors in config file: '+str(num_errors),'error')
else:
    log('Configuration file validated')
    apply_config(config)
#
# logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)s %(message)s')
#
# p = bork.Package('libapache2-mod-php')
# p.apply_config()
#
# file_path = '/var/www/html/index.php'
# file_source = 'templates/index.php.template'
# file_owner = 'root'
# #file_content = 'test123'
# file_group = 'root'
# file_mode = '777'
#
# f = bork.File(file_path,file_source,file_owner,file_group,file_mode)
# f.apply_config()
#
# f2 = bork.File('/etc/apache2/apache2.conf','templates/apache2.conf.template','root',notify='apache2')
# f2.apply_config()
#
# s = bork.Service('apache2')
#
# #s.get_service_status()
# s.set_service_status('started')
