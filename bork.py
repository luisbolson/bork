import logging
import json
import os
import hashlib
import uuid
import pwd,grp
import shutil
import apt
import subprocess
from subprocess import DEVNULL

def colour(text, colour):
    colours = {
        'reset': '\033[0m',
        'bold': '\033[01m',
        'red': '\033[31m',
        'green': '\033[32m',
        'orange': '\033[33m',
        'blue': '\033[34m',
        'purple': '\033[35m',
        'cyan': '\033[36m'
    }
    if colour in colours.keys():
        ret = colours[colour] + text + colours['reset']
    else:
        ret = text
    return ret

def text_highlight(text, hl_type):
    hl_types = {
        'obj': 'purple',
        'ok': 'green',
        'error': 'red',
        'warn': 'orange',
        'class': 'cyan',
        'change': 'cyan'
    }
    if hl_type in hl_types.keys():
        ret = colour(text, hl_types[hl_type])
    else:
        ret = text
    return ret

def text_obj(text):
    return text_highlight(text, 'obj')

def text_class(text):
    return text_highlight(text, 'class')

def text_ok():
    return text_highlight('[OK] ', 'ok')

def text_change():
    return text_highlight('[Change] ', 'change')

# def load_config(filename):
#     logging.debug('def load_config')
#     logging.debug("filename %s", filename)
#     # Loads Json config file
#     config = json.loads(open(filename).read())
#     logging.debug(json.dumps(config, indent=4))
#     for resource in config:
#         print(resource)

def get_file_md5(filename):
    'Calculates the MD5 hash for a given file'
    # Define blocksize/chunksize to be read at a time
    # This allows reading larger files
    bs = 32768
    # Create hashlib
    hasher = hashlib.md5()
    # Open file for read in binary mode
    sourcefile = open(filename, 'rb')
    while True:
        file_buffer = sourcefile.read(bs)
        # Nothing else to read? Leave the while
        if not file_buffer:
            break
        # Update the hasher
        hasher.update(file_buffer)
    # Return the MD5 sum
    return hasher.hexdigest()

def create_temp_file(content):
    # generates a unique filename on temp
    filename = '/tmp/bork_' + str(uuid.uuid4())
    # Add line break to file
    create_file(filename,content + '\n')
    return filename

def create_file(filename,content):
    f = open(filename,'x')
    f.write(content)
    f.close()

def delete_file(filename):
    os.remove(filename)

def log_global(status,class_name,object_name,text):
    if status:
        status_ret = text_highlight('['+status+'] ', status)
    else:
        status_ret = ''
    class_ret = text_highlight('['+class_name+'] ', 'class')
    object_ret = text_highlight(object_name + ' ', 'obj')
    return class_ret + object_ret + status_ret + text

class File:
    'File resource class'
    # Desired Properties from config have name cfg_
    # Other properties are either from current file or shared with config
    # fix template nonexistent

    def log(self,text='',status='ok'):
        class_name = self.__class__.__name__
        logging.info(log_global(status,class_name,self.path,text))

    def apply_config(self):
        self.get_file_info()
        self.set_config()
        change = 0
        'Apply config to file'
        #self.log('Starting process')
        # if file exist, check contents
        if self.exists:
            self.log('File exists. Checking contents')
            # If contents are the same, don't do anything
            if self.md5 == self.cfg_md5:
                self.log('Contents are the same')
            # If there are differences, replace the file with the temporary file
            else:
                self.log('Contents are different. Updating file','change')
                shutil.copyfile(self.cfg_file, self.path)
                self.log('File Updated')
                change = 1
        # If file doesn't exist create it
        else:
            self.log('File does not exist','change')
            os.makedirs(os.path.dirname(self.path), exist_ok=True)
            shutil.copyfile(self.cfg_file, self.path)
            self.log('File Created')
            change = 1

        # File ownership
        # If owner was set in config, and it is different from actual owner
        if (self.cfg_owner):
            if (self.cfg_owner != self.owner):
                # then change the udi
                self.log('File has different owner','change')
                uid = pwd.getpwnam(self.cfg_owner).pw_uid
                self.log('File owner changed')
                change = 1
            else:
                self.log('File has the same owner. No changes')
                uid = -1
        # If the owner weas not set, stay quiet
        else:
            # -1 passed to os.chown means no change
            uid = -1

        # If group was set in config, and it is different from actual group
        if (self.cfg_group):
            if (self.cfg_group != self.group):
                # then change the udi
                self.log('File has different group','change')
                gid = grp.getgrnam(self.cfg_group).gr_gid
                self.log('File group changed')
                change = 1
            else:
                self.log('File has the same group. No changes')
                gid = -1
        # If the owner was not set stay quiet
        else:
            # -1 passed to os.chown means no change
            gid = -1

                # Permission mode
        if (self.cfg_mode):
            if (self.cfg_mode != self.mode):
                self.log('File has different mode','change')
                os.chmod(self.path, self.cfg_mode)
                self.log('File mode changed')
                change = 1
            else:
                self.log('File has the same mode. No changes')

        # If any of the IDs is positive it means there is an ownership change
        if (uid >= 0) or (gid >= 0):
            # Then run chown
            os.chown(self.path, uid, gid)
        if self.cfg_temp_file:
            delete_file(self.cfg_temp_file)

        if change and self.cfg_notify:
            self.log('File changed. Notifying service '+ self.cfg_notify +' to restart')
            s = Service(self.cfg_notify,log=False)
            sr = s.restart_service(staydown=True)
            if not sr:
                self.log('Service '+ self.cfg_notify +' restarted')
            elif sr == 2:
                self.log('Service '+ self.cfg_notify +'was already stopped, not starting up','warn')
            else:
                self.log("Service couldn't be restarted",'error')

    def set_config(self):
        'Compute configuration properties'
        # Check if using a source file or string for content, and hashing
        # If content is set, then content is string, not a file
        # If both are set, content gets preference
        if self.cfg_content:
            # Thus a temp file needs to be created with the contents from the JSON file
            self.cfg_temp_file = create_temp_file(self.cfg_content)
            self.cfg_file = self.cfg_temp_file
        # if there is no content on cfg and a source file is specified
        elif self.cfg_source:
            # Then the cfg_file is the source file
            self.cfg_file = self.cfg_source
        else:
            self.cfg_temp_file = create_temp_file('')
            self.cfg_file = self.cfg_temp_file
        # Get md5 from the file
        self.cfg_md5 = get_file_md5(self.cfg_file)

    def get_file_info(self):
        'Get the current attributes for the file in disk'
        # Check if file exists
        self.exists = os.path.isfile(self.path)
        logging.debug('File exists => ' + str(self.exists))
        # If file exists, lets get data about it
        if self.exists:
            # Calculate MD5 of file. This will be use for comparison
            self.md5 = get_file_md5(self.path)
            # Get file metadata
            file_mdata = os.stat(self.path)
            # Get mode, convert to oct and take last 4 chars to have permissions chmod numeric style
            self.mode = int(oct(file_mdata.st_mode)[-4:],8)
            # Get Owner and Group ID and transform to name
            #self.uid = file_mdata.st_uid
            #self.gid = file_mdata.st_gid
            self.owner = pwd.getpwuid(file_mdata.st_uid).pw_name
            self.group = grp.getgrgid(file_mdata.st_gid).gr_name
            logging.debug((str(self.mode) + ' ' + self.owner + ' ' + self.group))


    def __init__(self,path,source = None,owner = None,group = None,mode = None, content = None,notify = None):
        logging.debug('class File __init__')
        self.path = path
        self.exists = None
        self.owner = None
        self.mode = None
        self.group = None
        self.md5 = None
        self.cfg_content = content
        self.cfg_source = source
        self.cfg_file = None
        self.cfg_temp_file = None
        self.cfg_owner = owner
        self.cfg_group = group
        self.cfg_mode = mode
        self.cfg_notify = notify

class Package:
    'File resource class'
    # Desired Properties from config have name cfg_
    # Other properties are either from current file or shared with config

    def log(self,text='',status='ok'):
        class_name = self.__class__.__name__
        logging.info(log_global(status,class_name,self.name,text))

    def apply_config(self):
        change = 0
        self.package = self.cache[self.name]
        self.log('Package shoud be: ' + self.cfg_state)
        if self.package.is_installed:
            self.state = 'installed'
            if self.cfg_state == 'absent':
                self.log('Package is present. Removing', 'change')
                self.package.mark_delete()
                self.cache.commit()
                change = 1
            else:
                self.log('Already installed')
        else:
            self.state = 'absent'
            if self.cfg_state == 'installed':
                self.log('Package is not present. Installing', 'change')
                self.package.mark_install()
                self.cache.commit()
                self.log('Package installed')
                change = 1
            else:
                self.log('Package was already not installed')
        self.cache.close()

        if change and self.cfg_notify:
            self.log('Package changed. Notifying service '+ self.cfg_notify +' to restart')
            s = Service(self.cfg_notify,log=False)
            sr = s.restart_service(staydown=True)
            if not sr:
                self.log('Service '+ self.cfg_notify +' restarted')
            elif sr == 2:
                self.log('Service '+ self.cfg_notify +'was already stopped, not starting up','warn')
            else:
                self.log("Service couldn't be restarted",'error')

    def __init__(self, name=None, state='installed', notify=None):
        logging.debug('class Package __init__')
        self.name = name
        self.state = None
        self.cfg_state = state
        self.package = None
        self.cache = apt.cache.Cache()
        self.cache.update()
        self.cache.open(None)
        self.cfg_notify = notify
        if self.name not in self.cache:
            self.log('Package is not available in repository', 'error')


class Service:
    'Linux Service Class'

    def log(self,text='',status='ok'):
        class_name = self.__class__.__name__
        if self.logger:
            logging.info(log_global(status,class_name,self.name,text))

    def is_init_upstart(self):
        # if /sbin/initctl exists and --version returns upstart...
        if os.access('/sbin/initctl', os.X_OK):
            init_ouput = str(subprocess.check_output(['/sbin/initctl', '--version']))
            if 'upstart' in init_ouput:
                return True
        return False

    def is_init_systemd(self):
        # If runtime dir /run/systemd/system exists then we are using systemd
        if os.path.isdir('/run/systemd/system'):
            return True
        return False

    def get_init_system(self):
        if self.is_init_upstart():
            return 'upstart'
        elif self.is_init_systemd():
            return 'systemd'
        else:
            return 'sysvinit'

    def get_service_status(self):
        # service command is a wrapper script that works for upstart, sysvinit and systemd
        # The exit code depends on the init type
        # R = running, S = stopped, N = non existent
        #          R S N
        # upstart  0 3 1
        # systemd  0 3 3
        # sysvinit 0 3 1
        svc_exit_status = subprocess.call(["/usr/sbin/service",self.name,"status"],stdout=DEVNULL, stderr=subprocess.STDOUT)
        # Exit 0 means running
        if svc_exit_status == 0:
            return 'started'
        elif svc_exit_status == 3:
            # If systemd we need to clarify the 3 means stopped or not enabled
            if self.init_type == 'systemd':
                not_enabled = subprocess.call(["/bin/systemctl","is-enabled",self.name],stdout=DEVNULL, stderr=subprocess.STDOUT)
                # If return is 1, service is not enabled, likelly not installed (improvement here)
                if not_enabled:
                    return 'nonexistent'
            # If init not systemd 3 means stopped
            return 'stopped'
        # Remaining options are nonexistent service
        else:
            return 'nonexistent'

    def start_service(self):
        return subprocess.call(["/usr/sbin/service",self.name,"start"],stdout=DEVNULL)

    def stop_service(self):
        return subprocess.call(["/usr/sbin/service",self.name,"stop"],stdout=DEVNULL)

    def restart_service(self,staydown):
        # If staydown flag is set and the service is already down the service doesn't start
        if staydown and self.get_service_status() == 'stopped':
            return 2
        # If staydown is not set, it will try to stop/start regardless of previous state
        if not self.set_service_status('stopped'):
            if not self.set_service_status('started'):
                return 0
        else:
            return 1

    def set_service_status(self,status):
        current_status = self.get_service_status()
        if current_status == 'nonexistent':
            self.log('Service does not exist','error')
            return 1
        self.log('Service status should be: ' + status)
        if current_status == status:
            self.log('Service already ' + status)
            return 0
        elif status == 'started':
            self.log('Starting service','change')
            if not self.start_service():
                self.log('Service started')
                return 0
            else:
                self.log('Could not start service. See error above','error')
                return 1
        elif status == 'stopped':
            self.log('Stopping service')
            if not self.stop_service():
                self.log('Service stopped')
                return 0
        return 1

    def apply_config(self):
        return self.set_service_status(self.cfg_status)

    def __init__(self,name,status='started',log=True):
        self.name = name
        self.cfg_status = status
        self.logger = log
        self.init_type = self.get_init_system()
        #self.log('Process Start...')

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)s %(message)s')

    p = Package('libapache2-mod-php')
    p.apply_config()

    file_path = '/var/www/html/index.php'
    file_source = 'templates/index.php.template'
    file_owner = 'root'
    #file_content = 'test123'
    file_group = 'root'
    file_mode = '777'

    f = File(file_path,file_source,file_owner,file_group,file_mode)
    f.apply_config()

    f2 = File('/etc/apache2/apache2.conf','templates/apache2.conf.template','root',notify='apache2')
    f2.apply_config()



#    p1 = Package('apache2')
#    p1.apply_config()


    s = Service('apache')

    #s.get_service_status()
    s.set_service_status('started')
