# Bork #

Bork is an experimental configuration management system, inspired on the likes of chef and puppet. "Bork bork bork" is a phrase by the **puppet** Swedish **Chef**, from the The Muppet Show.

It allows basic management of
- Static Files
- Packages
- Services

The configuration is stored in a YAML file that is passed to bork_apply as a parameter

## Getting Started ##
Currently Bork works in Ubuntu Linux systems.
### Obtaining ###
Clone from the repo (if I happened to have given you access):
```
git clone https://bitbucket.org/luisbolson/bork.git
```

Or untar the file I sent you :)

### Installation ###

cd into untar/repo folder and run the bootstrap
```
cd bork
sudo ./bootstrap.sh
```

### Running ###
From anywhere
```
sudo bork_apply configfile.yaml
```

## Configuration File ##
The basic configuration format is a YAML like this:
```
ObjectName:
    type    : ObjectType
    param_1 : value
    ...
    param_n : value
```

ObjectName is the name of package, service or path of file to be configured

Example:
```
apache2:
    type    : package
    state   : installed
```
Besides the ObjectName, the only mandatory parameter is type.

## Configuration Parameters ##

### File ###
| Parameter | Description |
| --- | --- |
| type | Type of Object. needs to be 'file' |
| source | Path for a file to be used as template. If content on destination is different, bork will replace with contents of *source* |
| content | Text contents you want the file to have. ** If both content and source are set, content takes precedence** |
| owner | File owner |
| group | File group |
| mode | File permissions in numeric format (4 digits) |
| notify | Name of the service to be notified in case of changes in the file. Service will be restarted if already running. If service is down it will not attempt to startup.|

#### File example 1 ####
```
/etc/apache2/apache2.conf:
    type    : file
    source  : templates/apache2.conf.template
    owner   : root
    group   : root
    mode    : 0777
    notify  : apache2
```

#### File example 2 ####
```
/var/www/html/index.php:
    type    : file
    content : '<?php
    header("Content-Type: text/plain");
    echo "Hello, world!\n";'
```

### Package ###
| Parameter | Description |
| --- | --- |
| type | Type of Object. needs to be 'package' |
| state | Possible states: **installed** or **absent**. Defaults to **installed** |
| notify | Service name to be restarted (if already running). Doesn't need to be a service specified in the bork config file. It can be any service accessible through the wrapper /usr/sbin/service |

#### Package example ####
```
libapache2-mod-php7.0:
    type    : package
    state   : installed
    notify  : apache2
```

### Service ###
| Parameter | Description |
| --- | --- |
| type | Type of Object. needs to be 'service' |
| status | Service Statuts. **stopped** or **started** |

#### Service example ####
```
apache2:
    type    : service
    status  : stopped
```
